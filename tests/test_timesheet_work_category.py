# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest

import doctest

from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
from trytond.tests.test_tryton import doctest_teardown
from trytond.tests.test_tryton import doctest_checker
from trytond.modules.company.tests import create_company, set_company
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.tests.tools import activate_modules


class TimesheetWorkCategoryTestCase(ModuleTestCase):
    """Test Timesheet Work Category module"""
    module = 'timesheet_work_category'

    @with_transaction()
    def test_category(self):
        'Create category'
        pool = Pool()
        Category = pool.get('timesheet.work.category')
        category1, = Category.create([{
                    'name': 'Category 1',
                    }])
        self.assertTrue(category1.id)

    @with_transaction()
    def test_category_recursion(self):
        'Test category recursion'
        pool = Pool()
        Category = pool.get('timesheet.work.category')
        category1, = Category.create([{
                    'name': 'Category 1',
                    }])
        category2, = Category.create([{
                    'name': 'Category 2',
                    'parent': category1.id,
                    }])
        self.assertTrue(category2.id)

        self.assertRaises(Exception, Category.write, [category1], {
                'parent': category2.id,
                })

    @with_transaction()
    def test_work_with_category(self):
        'Create work with category'
        company = create_company()
        with set_company(company):
            pool = Pool()
            Work = pool.get('timesheet.work')
            Category = pool.get('timesheet.work.category')
            category1, = Category.create([{
                        'name': 'Category 1',
                        }])
            work = Work(name='Work 1', categories=())
            work.categories = [category1]
            work.save()
            self.assertEqual(len(work.categories), 1)


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            TimesheetWorkCategoryTestCase))
    return suite
