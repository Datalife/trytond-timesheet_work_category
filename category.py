# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import (ModelSQL, ModelView, fields, Unique,
    DeactivableMixin, tree)
from trytond.exceptions import UserError
from trytond.i18n import gettext

SEPARATOR = ' / '


class Category(tree(), DeactivableMixin, ModelSQL, ModelView):
    '''Category'''
    __name__ = 'timesheet.work.category'
    name = fields.Char('Name', required=True)
    parent = fields.Many2One('timesheet.work.category', 'Parent',
        select=True)
    childs = fields.One2Many('timesheet.work.category', 'parent',
       'Children')

    @classmethod
    def __setup__(cls):
        super(Category, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_parent_uniq', Unique(t, t.name, t.parent),
                'timesheet_work_category.'
                'msg_timesheet_work_category_name_parent_uniq'),
            ]

    @classmethod
    def validate(cls, categories):
        super(Category, cls).validate(categories)
        for category in categories:
            category.check_name()

    def check_name(self):
        if SEPARATOR in self.name:
            raise UserError(gettext(
                'timesheet_work_category.'
                'msg_timesheet_work_category_wrong_name',
                category=self.name))

    def get_rec_name(self, name):
        if self.parent:
            return self.parent.get_rec_name(name) + SEPARATOR + self.name
        return self.name

    @classmethod
    def search_rec_name(cls, name, clause):
        if isinstance(clause[2], str):
            values = clause[2].split(SEPARATOR)
            values.reverse()
            domain = []
            field = 'name'
            for name in values:
                domain.append((field, clause[1], name))
                field = 'parent.' + field
            return domain
        return [('name',) + tuple(clause[1:])]
