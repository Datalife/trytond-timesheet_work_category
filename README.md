datalife_timesheet_work_category
================================

The timesheet_work_category module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-timesheet_work_category/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-timesheet_work_category)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
